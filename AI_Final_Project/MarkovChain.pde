import java.util.*;

PrintWriter output;
BufferedReader reader;
int lineCount = 0;
int order = 3;
String line;
String[] tokens;
ArrayList<String> res = new ArrayList<String>();
HashMap<String, MarkovChain> dictionary = new HashMap<String, MarkovChain>();

class MarkovChain{
  
  ArrayList ngrams;
  
  MarkovChain(){
    ngrams = new ArrayList();
  }
  
  ArrayList getNgrams() {
    return ngrams;
  }
  
  void setNgrams(ArrayList list) {
    this.ngrams = list;
  }
}

void setup() {
  
  output = createWriter("output.txt");

  loadDictionary();
  loadInput();
  markov();
  saveDictionary();
  
  print("done!");
  exit();

}

// load input file
void loadInput() {

  reader = createReader("text.txt");
  
  try {
    line = reader.readLine();
  } catch (IOException e) {
    e.printStackTrace();
    line = null;
    exit();
  }
  
  tokens = splitTokens(line);
  int len = tokens.length;
  for (int i=0; i < len-order ; i++) {
    String temp = tokens[i];
    res.add(tokens[i]);
    
    // create ngrams
    for (int j=1; j < order ; j++) {
      temp += " " + tokens[i+j];
    }
    
    ArrayList curr;
    MarkovChain mc = dictionary.get(temp);

    // if ngram did not exist, create a new one
    if (mc == null) {
      mc = new MarkovChain();
    }
    
    // add into list of possibilities
    curr = mc.getNgrams();
    curr.add(tokens[i+order]);
    mc.setNgrams(curr);
    
    dictionary.put(temp, mc);

  }

}

// load knowledge list
void loadDictionary() {
  BufferedReader inputDict = createReader("dictionary.txt");
  
  // if dictionary did not exist, skip reading
  if (inputDict != null) {
  
    while(true) { 
      try {
        line = inputDict.readLine();
      } catch (IOException e) {
        e.printStackTrace();
        line = null;
        exit();
      }
      if (line == null)
        break;
        
      // split key and value
      tokens = splitTokens(line, ";");

      String ngram = tokens[0];                            // key
      ArrayList<String> val = new ArrayList<String>();     // value

      tokens = splitTokens(tokens[1], ",");
      for (int i=0; i<tokens.length; i++) {
        val.add(tokens[i]);
      }
      MarkovChain mc = new MarkovChain();
      mc.setNgrams(val);
      dictionary.put(ngram, mc);
    }
    
  }

}

// update knowledge list
void saveDictionary() {
  PrintWriter newDict = createWriter("dictionary.txt");
  
  for (String key : dictionary.keySet()) {
    ArrayList<String> temp = dictionary.get(key).getNgrams();
    newDict.print(key + ";");
    newDict.print(temp.get(0));
    for (int i=1; i<temp.size(); i++) {
       newDict.print("," + temp.get(i));
    }
    newDict.println("");
  }
  newDict.flush();
  newDict.close();
}


// generates result using markov chain
void markov() {
  
  String currentGram = substr(res, 0, order);
  ArrayList result = new ArrayList();
  for (int i=0; i<3; i++) {
    result.add(tokens[i]);
  }
  
  output.print(currentGram + " ");

  for (int i=0; i<100; i++) {
    MarkovChain mc = dictionary.get(currentGram);
    
    // if no more possibility, break
    if (mc == null)
      break;
    
    ArrayList<String> temp = mc.getNgrams();
    int rand = int(random(temp.size()));
    String chosen = temp.get(rand);

    result.add(chosen);
    
    // print to output file
    output.print(chosen + " ");
    
    currentGram = substr(result, i+1, order);
  }
  
  //println(result);
  
  output.flush();
  output.close();
}

// create a string of 3 space separated strings from arraylist
String substr(ArrayList<String> arr, int start, int len) {
  String result = arr.get(start);
  int end = start + len;
  
  for (int i=start+1; i<end; i++) {
    result += " " + arr.get(i);
  }
  
  return result;
}

void draw() {
  // empty
}
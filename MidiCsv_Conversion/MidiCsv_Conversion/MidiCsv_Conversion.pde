import java.util.regex.*;

int mode = 1;
int lineCount = 0;
Notes[][] notes = new Notes[150000][104];
PrintWriter output;
BufferedReader reader;
String line;
boolean skipRest = false;
int time = 0;

class Notes {
  int note = 0;
  boolean play = false;
  Notes(int n, boolean p){
    note = n;
    play = p;
  }
}

boolean isInstrument(String input){
  switch(input){
    case "Poly_aftertouch_c":
    case "Note_off_c":
    case "Note_on_c": return true;
    default: return false;
  }
}

void setup(){
  if(mode == 0){
    reader = createReader("tst.csv");
    output = createWriter("converted.txt");
  }else{
    reader = createReader("converted.txt");
    output = createWriter("new.csv");
  }
}

void draw(){
  try{
    line = reader.readLine();
  } catch (IOException e) {
    e.printStackTrace();
    line = null;
  }
  
  if(mode == 0){
    if (line == null || skipRest) {
      // Stop reading because of an error or file is empty
      noLoop();
      int notesLength = notes.length;
      int channelSize = notes[0].length;
      for(int i = 0; i < notesLength; i++){
        if(notes[i][0] == null)
          continue;
          
        String tmpS = "";
        
        for(int j = 0; j < channelSize; j++){
          Notes selected = notes[i][j];
          if(selected == null)
            continue;
            
          if(!selected.play) // Skip check if note is not playing
            continue;
          
          /* Find stop play for the following note */
          int playTime = 0;
          
          outerloop:
          for(int t = i; t < notesLength; t++){
            for(int n = 0; n < channelSize; n++){
              Notes toCheck = notes[t][n];
              
              if(toCheck == null)
                continue;
              
              if(!toCheck.play && toCheck.note == selected.note){
                playTime = t - i;
                break outerloop;
              }
            }
          }
          
          tmpS += String.valueOf((char) (33 + (selected.play ? 0 : 1)));
          tmpS += playTime;
          
          tmpS += String.valueOf((char) (128 + selected.note));
          //println(tmpS + " ");
        }
        
        output.print(tmpS + " ");
        //println(tmpS + " ");
      }
      output.flush();
      output.close();
      exit();
    } else {
      String[] pieces = split(line, ", ");
      if(isInstrument(pieces[2])){
        boolean isPlaying = Integer.parseInt(pieces[5]) > 0;
        int midiNumber = Integer.parseInt(pieces[4]);
        
        if(Integer.parseInt(pieces[3]) == 0){
          for(int i = 0; i < notes[Integer.parseInt(pieces[1])].length; i++){
            if(notes[Integer.parseInt(pieces[1])][i] == null){
              notes[Integer.parseInt(pieces[1])][i] = new Notes(midiNumber, isPlaying);
              break;
            }
          }
        }else if(Integer.parseInt(pieces[3]) == 1)
          skipRest = true;
      }
      println(lineCount);
      lineCount++;
    }
  }else{
    if(time == 0){
      output.println("0, 0, Header, 1, 2, 960");
      output.println("1, 0, Start_track");
      output.println("1, 0, Tempo, 1200000");
      output.println("1, 0, Key_signature, 0, \"major\"");
      output.println("1, 0, Title_t, \"test\"");
      output.println("1, 0, Time_signature, 4, 2, 24, 8");
    }
    
    String[] pieces = split(line, " ");
    String finalBody = "";
    for(int i = 0; i < pieces.length; i++){
      String selected = pieces[i];
      int note = 0;
      int timeLength = 0;
        
      try{
        if(((int)selected.charAt(0)) != 33)
          continue;
        
        note = (int)selected.charAt(selected.length() - 1) - 128;
        timeLength = Integer.parseInt(selected.substring(1, selected.length() - 1));
        
        if(timeLength == 0)
          continue;
        
        finalBody += "2, "+time+", Note_on_c, 0, "+note+", 100\n";
        time += timeLength;
        finalBody += "2, "+time+", Note_off_c, 0, "+note+", 0\n";
      }catch(Exception e){
        // Pretend like nothing happened. Everything is fine...
      }
    }
    
    //output.println("1, "+time+", Marker_t, \"V.1\"");
    output.println("1, 0, End_track");
    
    output.println("2, 0, Start_track");
    output.println("2, 0, Control_c, 0, 0, 0");
    output.println("2, 0, Control_c, 0, 32, 0");
    output.println("2, 0, Program_c, 0, 0");
    output.println("2, 0, Title_t, \"Untitled\"");
    output.println("2, 0, Control_c, 0, 7, 127");
    
    output.print(finalBody);
    
    output.println("2, "+time+", End_track");
    output.println("0, 0, End_of_file");
    output.flush();
    output.close();
    exit();
  }
}